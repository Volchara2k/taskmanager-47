package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.entity.Task;

import java.util.UUID;

public final class CaseDataTaskProvider {

    @NotNull
    public static Task createTask() {
        return new Task(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()
        );
    }

}