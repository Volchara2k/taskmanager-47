package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.WebApplication;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserRoleService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.entity.User;
import ru.renessans.jvschool.volkov.task.manager.entity.UserRole;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@Setter
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class)
@Category({PositiveImplementation.class, ServiceImplementation.class})
public class RoleServiceTest {

    @NotNull
    @Autowired
    private IUserRoleService userRoleService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Test
    public void addRoleTest() {
        @NotNull final User user = new User(
                UUID.randomUUID().toString(), UUID.randomUUID().toString()
        );
        Assert.assertNotNull(user);
        @NotNull final User saveUserResponse = this.userService.save(user);
        Assert.assertNotNull(saveUserResponse);

        @NotNull final Set<UserRole> userRoleSetResponse = this.userRoleService.addRole(
                saveUserResponse.getId(), Collections.singleton(UserRoleType.MANAGER)
        );
        Assert.assertNotNull(userRoleSetResponse);
        Assert.assertNotEquals(0, userRoleSetResponse.size());
    }

}