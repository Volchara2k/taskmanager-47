package ru.renessans.jvschool.volkov.task.manager.controller.view;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.renessans.jvschool.volkov.task.manager.AbstractWebApplicationTest;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.marker.ControllerImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Category({PositiveImplementation.class, ControllerImplementation.class})
public class ProjectControllerTest extends AbstractWebApplicationTest {

    @Test
    public void projectsTest() throws Exception {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/projects"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("project/project-list"));
    }

    @Test
    public void viewTest() throws Exception {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/project/view/{id}", PROJECT.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("project/project-view"));
    }

    @Test
    public void createGetTest() throws Exception {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/project/create"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("project/project-settable"));
    }

    @Test
    public void createPostTest() throws Exception {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setTitle("title");
        projectDTO.setDescription("description");
        projectDTO.setUserId(USER.getId());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/project/create")
                .flashAttr("project", projectDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects"));
    }

    @Test
    public void removeTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/project/delete/{id}", PROJECT.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects"));
    }

    @Test
    public void editGetTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/project/edit/{id}", PROJECT.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("project/project-settable"));
    }

    @Test
    public void editPostTest() throws Exception {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setTitle("title");
        projectDTO.setDescription("description");
        projectDTO.setUserId(USER.getId());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/project/edit/{id}", PROJECT.getId())
                .flashAttr("project", projectDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/project/view/{id}"));
    }

}