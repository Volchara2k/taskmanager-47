package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.WebApplication;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.entity.User;
import ru.renessans.jvschool.volkov.task.manager.entity.UserRole;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Setter
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class)
@Category({PositiveImplementation.class, ServiceImplementation.class})
public class UserServiceTest {

    @NotNull
    @Autowired
    private IUserService userService;

    @Test
    public void testAddUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final User addUserResponse = this.userService.addUser(login, UUID.randomUUID().toString());
        Assert.assertNotNull(addUserResponse);
        Assert.assertEquals(login, addUserResponse.getLogin());
    }

    @Test
    public void testAddUserWithFirstName() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String firstName = UUID.randomUUID().toString();
        @NotNull final User addUserResponse = this.userService.addUser(login, UUID.randomUUID().toString(), firstName);
        Assert.assertNotNull(addUserResponse);
        Assert.assertEquals(login, addUserResponse.getLogin());
        Assert.assertEquals(firstName, addUserResponse.getFirstName());
    }

    @Test
    public void testAddUserWithRole() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final Set<UserRoleType> roles = new HashSet<>(Collections.singleton(UserRoleType.MANAGER));
        @NotNull final User addUserResponse = this.userService.addUser(login, UUID.randomUUID().toString(), roles);
        Assert.assertNotNull(addUserResponse);
        Assert.assertEquals(login, addUserResponse.getLogin());
        Assert.assertEquals(
                roles, addUserResponse.getRoles()
                        .stream()
                        .map(UserRole::getUserRole)
                        .collect(Collectors.toSet())
        );
    }

}