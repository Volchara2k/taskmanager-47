package ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;

@SuppressWarnings("unused")
public interface IAuthenticationSoapEndpoint {

    @WebMethod
    @WebResult(name = "loginFlag", partName = "loginFlag")
    boolean login(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password
    );

    @WebMethod
    @WebResult(name = "logoutFlag", partName = "logoutFlag")
    boolean logout();

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    UserLimitedDTO signUpUser(
            @Nullable String login,
            @Nullable String password
    );

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    UserLimitedDTO signUpUserWithFirstName(
            @Nullable String login,
            @Nullable String password,
            @Nullable String firstName
    );

}