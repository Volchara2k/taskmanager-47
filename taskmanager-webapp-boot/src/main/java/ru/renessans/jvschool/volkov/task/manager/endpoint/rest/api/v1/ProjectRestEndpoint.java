package ru.renessans.jvschool.volkov.task.manager.endpoint.rest.api.v1;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.rest.IProjectRestEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(
        value = "/api",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @NotNull
    private final IUserProjectService userProjectService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @NotNull
    @GetMapping("/projects")
    @ApiOperation(
            value = "Get all projects",
            notes = "Returns a complete list of project details by order of creation."
    )
    @Override
    public Collection<ProjectDTO> getAllProjects() {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        return this.userProjectService.getUserOwnerAll(userid)
                .stream()
                .map(this.projectAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    @ApiOperation(
            value = "Get project by ID",
            notes = "Returns project by unique ID. Unique ID required."
    )
    @Override
    public ProjectDTO getProjectById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of project",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull final String id
    ) {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        @Nullable final Project project = this.userProjectService.getUserOwnerById(userid, id);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        return this.projectAdapterService.toDTO(project);
    }

    @Nullable
    @RequestMapping(
            value = "/project/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(
            value = "Create project",
            notes = "Returns created project. Created project required."
    )
    @Override
    public ProjectDTO createProject(
            @ApiParam(
                    name = "projectDTO",
                    type = "ProjectDTO",
                    value = "Created project",
                    required = true
            )
            @RequestBody @NotNull final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        if (Objects.isNull(project)) return null;
        @NotNull final String userid = CurrentUserUtil.getUserId();
        project.setUserId(userid);
        try {
            this.userProjectService.addUserOwner(project);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return projectDTO;
    }

    @DeleteMapping("/project/delete/{id}")
    @ApiOperation(
            value = "Delete project by id",
            notes = "Returns integer deleted flag: 1 - true, 0 - false. Unique ID required."
    )
    @Override
    public int deleteProjectById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of project",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull final String id
    ) {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        return this.userProjectService.deleteUserOwnerById(userid, id);
    }

    @Nullable
    @SneakyThrows
    @RequestMapping(
            value = "/project/edit",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(
            value = "Edit project",
            notes = "Returns edited project. Edited project required."
    )
    @Override
    public ProjectDTO editProject(
            @ApiParam(
                    name = "projectDTO",
                    type = "ProjectDTO",
                    value = "Edited project",
                    required = true
            )
            @RequestBody @NotNull final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        @NotNull final String userid = CurrentUserUtil.getUserId();
        if (ValidRuleUtil.isNullOrEmpty(project.getUserId())) project.setUserId(userid);
        try {
            this.userProjectService.addUserOwner(project);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return projectDTO;
    }

}