package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.entity.Task;

public interface IUserTaskService extends IUserOwnerService<Task> {

    @NotNull
    Task addUserOwner(
            @Nullable Task task
    );

}