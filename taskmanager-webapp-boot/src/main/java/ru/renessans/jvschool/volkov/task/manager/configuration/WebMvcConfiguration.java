package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    @Override
    public void addViewControllers(@NotNull final ViewControllerRegistry viewControllerRegistry) {
        viewControllerRegistry
                .addViewController("/")
                .setViewName("/index");
        viewControllerRegistry
                .addViewController("/login")
                .setViewName("/security/login");
    }

    @Override
    public void addResourceHandlers(@NotNull final ResourceHandlerRegistry resourceHandlerRegistry) {
        resourceHandlerRegistry
                .addResourceHandler("/resources/**")
                .addResourceLocations("/resources/static/");
    }

}