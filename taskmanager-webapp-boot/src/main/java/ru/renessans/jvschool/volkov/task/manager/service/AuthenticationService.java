package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.entity.User;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidFirstNameException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidLoginException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidPasswordException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserRoleException;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Objects;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthenticationService implements IAuthenticationService {

    @NotNull
    private final IUserService userService;

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        return this.userService.addUser(login, password);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        return this.userService.addUser(login, password, firstName);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Set<UserRoleType> userRoleTypes
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        if (Objects.isNull(userRoleTypes)) throw new InvalidUserRoleException();
        return this.userService.addUser(login, password, userRoleTypes);
    }

}