package ru.renessans.jvschool.volkov.task.manager.api.endpoint.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;

import java.util.Collection;

public interface ITaskRestEndpoint {

    @NotNull
    @GetMapping
    @ApiOperation(
            value = "Get all projects",
            notes = "Returns a complete list of projects details by order of creation."
    )
    Collection<TaskDTO> getAllTasks();

    @Nullable
    @GetMapping("/task/view/{id}")
    @ApiOperation(
            value = "Get task by ID",
            notes = "Returns task by unique ID. Unique ID required."
    )
    TaskDTO getTaskById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of task",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull String id
    );

    @Nullable
    @RequestMapping(
            value = "/task/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(
            value = "Create task",
            notes = "Returns created task. Created task required."
    )
    TaskDTO createTask(
            @ApiParam(
                    name = "taskDTO",
                    type = "TaskDTO",
                    value = "Created project",
                    required = true
            )
            @RequestBody @NotNull TaskDTO taskDTO
    );

    @DeleteMapping("/task/delete/{id}")
    @ApiOperation(
            value = "Delete task by id",
            notes = "Returns integer deleted flag: 1 - true, 0 - false. Unique ID required."
    )
    int deleteTaskById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of task",
                    example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
                    required = true
            )
            @PathVariable("id") @NotNull String id
    );

    @Nullable
    @RequestMapping(
            value = "/task/edit",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(
            value = "Edit task",
            notes = "Returns edited task. Edited task required."
    )
    TaskDTO editTask(
            @ApiParam(
                    name = "taskDTO",
                    type = "TaskDTO",
                    value = "Edited project",
                    required = true
            )
            @RequestBody @NotNull TaskDTO taskDTO
    );

}