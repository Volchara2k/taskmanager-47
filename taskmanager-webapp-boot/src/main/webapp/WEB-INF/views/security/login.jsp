<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../../include/_header.jsp"/>

    <div class="container">
        <div class="login_activity-content">
            <h2>Страница аутентификации</h2>
        </div>

        <form name="f" action="/auth" method="POST">
            <div class="card">
                <div class="card-body">

                    <div class="write-content">
                        <div class="form-group row">
                            <label for="username" class="col-sm-2 col-form-label">Логин:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="username" placeholder="Введите логин" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-2 col-form-label">Пароль:</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control" name="password" placeholder="Введите пароль" />
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <button type="submit" name="submit" class="btn btn-primary" value="Login">Отправить</button>
                        </div>

                    </div>

                </div>
            </div>
        </form>

    </div>

<jsp:include page="../../include/_footer.jsp"/>