<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="task_write-content">
    <div class="card">
        <div class="card-body">

            <div class="form-group row">
                <label for="ProjectTitle" class="col-sm-2 col-form-label">Проект:</label>
                <div class="col-sm-7">
                    <form:select class="form-control" name="ProjectTitle" path="projectId">
                        <form:option value="${null}" label="NONE" />
                        <form:options items="${projects}" itemLabel="title" itemValue="id" />
                    </form:select>
                </div>
            </div>

            <jsp:include page="../../include/_input-form.jsp" />

        </div>
    </div>
</div>