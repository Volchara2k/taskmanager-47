package ru.renessans.jvschool.volkov.task.manager.listener.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.AbstractAdminListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class UserLockListener extends AbstractAdminListener {

    @NotNull
    private static final String CMD_USER_LOCK = "user-lock";

    @NotNull
    private static final String DESC_USER_LOCK = "заблокировать пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_LOCK =
            "Происходит попытка инициализации блокирования пользователя. \n" +
                    "Для блокирования пользователя введите его логин. ";

    public UserLockListener(
            @NotNull final AdminEndpoint adminEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_USER_LOCK;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_USER_LOCK;
    }

    @Async
    @Override
    @EventListener(condition = "@userLockListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_USER_LOCK);
        @NotNull final String login = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @NotNull final UserLimitedDTO lock = super.adminEndpoint.lockUserByLogin(current, login);
        ViewUtil.print(lock);
    }

}