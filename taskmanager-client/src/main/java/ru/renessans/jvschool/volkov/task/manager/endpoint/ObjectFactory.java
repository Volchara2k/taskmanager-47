
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.renessans.jvschool.volkov.task.manager.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddProject_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "addProject");
    private final static QName _AddProjectResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "addProjectResponse");
    private final static QName _DeleteAllProjects_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "deleteAllProjects");
    private final static QName _DeleteAllProjectsResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "deleteAllProjectsResponse");
    private final static QName _DeleteProjectById_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "deleteProjectById");
    private final static QName _DeleteProjectByIdResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "deleteProjectByIdResponse");
    private final static QName _DeleteProjectByIndex_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "deleteProjectByIndex");
    private final static QName _DeleteProjectByIndexResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "deleteProjectByIndexResponse");
    private final static QName _DeleteProjectByTitle_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "deleteProjectByTitle");
    private final static QName _DeleteProjectByTitleResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "deleteProjectByTitleResponse");
    private final static QName _GetAllProjects_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "getAllProjects");
    private final static QName _GetAllProjectsResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "getAllProjectsResponse");
    private final static QName _GetProjectById_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "getProjectById");
    private final static QName _GetProjectByIdResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "getProjectByIdResponse");
    private final static QName _GetProjectByIndex_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "getProjectByIndex");
    private final static QName _GetProjectByIndexResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "getProjectByIndexResponse");
    private final static QName _GetProjectByTitle_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "getProjectByTitle");
    private final static QName _GetProjectByTitleResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "getProjectByTitleResponse");
    private final static QName _UpdateProjectById_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "updateProjectById");
    private final static QName _UpdateProjectByIdResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "updateProjectByIdResponse");
    private final static QName _UpdateProjectByIndex_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "updateProjectByIndex");
    private final static QName _UpdateProjectByIndexResponse_QNAME = new QName("http://endpoint.manager.task.volkov.jvschool.renessans.ru/", "updateProjectByIndexResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.renessans.jvschool.volkov.task.manager.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddProject }
     * 
     */
    public AddProject createAddProject() {
        return new AddProject();
    }

    /**
     * Create an instance of {@link AddProjectResponse }
     * 
     */
    public AddProjectResponse createAddProjectResponse() {
        return new AddProjectResponse();
    }

    /**
     * Create an instance of {@link DeleteAllProjects }
     * 
     */
    public DeleteAllProjects createDeleteAllProjects() {
        return new DeleteAllProjects();
    }

    /**
     * Create an instance of {@link DeleteAllProjectsResponse }
     * 
     */
    public DeleteAllProjectsResponse createDeleteAllProjectsResponse() {
        return new DeleteAllProjectsResponse();
    }

    /**
     * Create an instance of {@link DeleteProjectById }
     * 
     */
    public DeleteProjectById createDeleteProjectById() {
        return new DeleteProjectById();
    }

    /**
     * Create an instance of {@link DeleteProjectByIdResponse }
     * 
     */
    public DeleteProjectByIdResponse createDeleteProjectByIdResponse() {
        return new DeleteProjectByIdResponse();
    }

    /**
     * Create an instance of {@link DeleteProjectByIndex }
     * 
     */
    public DeleteProjectByIndex createDeleteProjectByIndex() {
        return new DeleteProjectByIndex();
    }

    /**
     * Create an instance of {@link DeleteProjectByIndexResponse }
     * 
     */
    public DeleteProjectByIndexResponse createDeleteProjectByIndexResponse() {
        return new DeleteProjectByIndexResponse();
    }

    /**
     * Create an instance of {@link DeleteProjectByTitle }
     * 
     */
    public DeleteProjectByTitle createDeleteProjectByTitle() {
        return new DeleteProjectByTitle();
    }

    /**
     * Create an instance of {@link DeleteProjectByTitleResponse }
     * 
     */
    public DeleteProjectByTitleResponse createDeleteProjectByTitleResponse() {
        return new DeleteProjectByTitleResponse();
    }

    /**
     * Create an instance of {@link GetAllProjects }
     * 
     */
    public GetAllProjects createGetAllProjects() {
        return new GetAllProjects();
    }

    /**
     * Create an instance of {@link GetAllProjectsResponse }
     * 
     */
    public GetAllProjectsResponse createGetAllProjectsResponse() {
        return new GetAllProjectsResponse();
    }

    /**
     * Create an instance of {@link GetProjectById }
     * 
     */
    public GetProjectById createGetProjectById() {
        return new GetProjectById();
    }

    /**
     * Create an instance of {@link GetProjectByIdResponse }
     * 
     */
    public GetProjectByIdResponse createGetProjectByIdResponse() {
        return new GetProjectByIdResponse();
    }

    /**
     * Create an instance of {@link GetProjectByIndex }
     * 
     */
    public GetProjectByIndex createGetProjectByIndex() {
        return new GetProjectByIndex();
    }

    /**
     * Create an instance of {@link GetProjectByIndexResponse }
     * 
     */
    public GetProjectByIndexResponse createGetProjectByIndexResponse() {
        return new GetProjectByIndexResponse();
    }

    /**
     * Create an instance of {@link GetProjectByTitle }
     * 
     */
    public GetProjectByTitle createGetProjectByTitle() {
        return new GetProjectByTitle();
    }

    /**
     * Create an instance of {@link GetProjectByTitleResponse }
     * 
     */
    public GetProjectByTitleResponse createGetProjectByTitleResponse() {
        return new GetProjectByTitleResponse();
    }

    /**
     * Create an instance of {@link UpdateProjectById }
     * 
     */
    public UpdateProjectById createUpdateProjectById() {
        return new UpdateProjectById();
    }

    /**
     * Create an instance of {@link UpdateProjectByIdResponse }
     * 
     */
    public UpdateProjectByIdResponse createUpdateProjectByIdResponse() {
        return new UpdateProjectByIdResponse();
    }

    /**
     * Create an instance of {@link UpdateProjectByIndex }
     * 
     */
    public UpdateProjectByIndex createUpdateProjectByIndex() {
        return new UpdateProjectByIndex();
    }

    /**
     * Create an instance of {@link UpdateProjectByIndexResponse }
     * 
     */
    public UpdateProjectByIndexResponse createUpdateProjectByIndexResponse() {
        return new UpdateProjectByIndexResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link ProjectDTO }
     * 
     */
    public ProjectDTO createProjectDTO() {
        return new ProjectDTO();
    }

    /**
     * Create an instance of {@link TimeFrameDTO }
     * 
     */
    public TimeFrameDTO createTimeFrameDTO() {
        return new TimeFrameDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddProject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "addProject")
    public JAXBElement<AddProject> createAddProject(AddProject value) {
        return new JAXBElement<AddProject>(_AddProject_QNAME, AddProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddProjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "addProjectResponse")
    public JAXBElement<AddProjectResponse> createAddProjectResponse(AddProjectResponse value) {
        return new JAXBElement<AddProjectResponse>(_AddProjectResponse_QNAME, AddProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAllProjects }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "deleteAllProjects")
    public JAXBElement<DeleteAllProjects> createDeleteAllProjects(DeleteAllProjects value) {
        return new JAXBElement<DeleteAllProjects>(_DeleteAllProjects_QNAME, DeleteAllProjects.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAllProjectsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "deleteAllProjectsResponse")
    public JAXBElement<DeleteAllProjectsResponse> createDeleteAllProjectsResponse(DeleteAllProjectsResponse value) {
        return new JAXBElement<DeleteAllProjectsResponse>(_DeleteAllProjectsResponse_QNAME, DeleteAllProjectsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "deleteProjectById")
    public JAXBElement<DeleteProjectById> createDeleteProjectById(DeleteProjectById value) {
        return new JAXBElement<DeleteProjectById>(_DeleteProjectById_QNAME, DeleteProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "deleteProjectByIdResponse")
    public JAXBElement<DeleteProjectByIdResponse> createDeleteProjectByIdResponse(DeleteProjectByIdResponse value) {
        return new JAXBElement<DeleteProjectByIdResponse>(_DeleteProjectByIdResponse_QNAME, DeleteProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProjectByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "deleteProjectByIndex")
    public JAXBElement<DeleteProjectByIndex> createDeleteProjectByIndex(DeleteProjectByIndex value) {
        return new JAXBElement<DeleteProjectByIndex>(_DeleteProjectByIndex_QNAME, DeleteProjectByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProjectByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "deleteProjectByIndexResponse")
    public JAXBElement<DeleteProjectByIndexResponse> createDeleteProjectByIndexResponse(DeleteProjectByIndexResponse value) {
        return new JAXBElement<DeleteProjectByIndexResponse>(_DeleteProjectByIndexResponse_QNAME, DeleteProjectByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProjectByTitle }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "deleteProjectByTitle")
    public JAXBElement<DeleteProjectByTitle> createDeleteProjectByTitle(DeleteProjectByTitle value) {
        return new JAXBElement<DeleteProjectByTitle>(_DeleteProjectByTitle_QNAME, DeleteProjectByTitle.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProjectByTitleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "deleteProjectByTitleResponse")
    public JAXBElement<DeleteProjectByTitleResponse> createDeleteProjectByTitleResponse(DeleteProjectByTitleResponse value) {
        return new JAXBElement<DeleteProjectByTitleResponse>(_DeleteProjectByTitleResponse_QNAME, DeleteProjectByTitleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllProjects }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getAllProjects")
    public JAXBElement<GetAllProjects> createGetAllProjects(GetAllProjects value) {
        return new JAXBElement<GetAllProjects>(_GetAllProjects_QNAME, GetAllProjects.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllProjectsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getAllProjectsResponse")
    public JAXBElement<GetAllProjectsResponse> createGetAllProjectsResponse(GetAllProjectsResponse value) {
        return new JAXBElement<GetAllProjectsResponse>(_GetAllProjectsResponse_QNAME, GetAllProjectsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getProjectById")
    public JAXBElement<GetProjectById> createGetProjectById(GetProjectById value) {
        return new JAXBElement<GetProjectById>(_GetProjectById_QNAME, GetProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getProjectByIdResponse")
    public JAXBElement<GetProjectByIdResponse> createGetProjectByIdResponse(GetProjectByIdResponse value) {
        return new JAXBElement<GetProjectByIdResponse>(_GetProjectByIdResponse_QNAME, GetProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getProjectByIndex")
    public JAXBElement<GetProjectByIndex> createGetProjectByIndex(GetProjectByIndex value) {
        return new JAXBElement<GetProjectByIndex>(_GetProjectByIndex_QNAME, GetProjectByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getProjectByIndexResponse")
    public JAXBElement<GetProjectByIndexResponse> createGetProjectByIndexResponse(GetProjectByIndexResponse value) {
        return new JAXBElement<GetProjectByIndexResponse>(_GetProjectByIndexResponse_QNAME, GetProjectByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectByTitle }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getProjectByTitle")
    public JAXBElement<GetProjectByTitle> createGetProjectByTitle(GetProjectByTitle value) {
        return new JAXBElement<GetProjectByTitle>(_GetProjectByTitle_QNAME, GetProjectByTitle.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectByTitleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "getProjectByTitleResponse")
    public JAXBElement<GetProjectByTitleResponse> createGetProjectByTitleResponse(GetProjectByTitleResponse value) {
        return new JAXBElement<GetProjectByTitleResponse>(_GetProjectByTitleResponse_QNAME, GetProjectByTitleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "updateProjectById")
    public JAXBElement<UpdateProjectById> createUpdateProjectById(UpdateProjectById value) {
        return new JAXBElement<UpdateProjectById>(_UpdateProjectById_QNAME, UpdateProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "updateProjectByIdResponse")
    public JAXBElement<UpdateProjectByIdResponse> createUpdateProjectByIdResponse(UpdateProjectByIdResponse value) {
        return new JAXBElement<UpdateProjectByIdResponse>(_UpdateProjectByIdResponse_QNAME, UpdateProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "updateProjectByIndex")
    public JAXBElement<UpdateProjectByIndex> createUpdateProjectByIndex(UpdateProjectByIndex value) {
        return new JAXBElement<UpdateProjectByIndex>(_UpdateProjectByIndex_QNAME, UpdateProjectByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "updateProjectByIndexResponse")
    public JAXBElement<UpdateProjectByIndexResponse> createUpdateProjectByIndexResponse(UpdateProjectByIndexResponse value) {
        return new JAXBElement<UpdateProjectByIndexResponse>(_UpdateProjectByIndexResponse_QNAME, UpdateProjectByIndexResponse.class, null, value);
    }

}
