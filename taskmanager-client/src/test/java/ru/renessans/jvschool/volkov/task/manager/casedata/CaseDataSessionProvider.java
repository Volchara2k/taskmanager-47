package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;

import java.util.UUID;

public final class CaseDataSessionProvider {

    @NotNull
    public static SessionDTO createSession() {
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        Assert.assertNotNull(sessionDTO);
        sessionDTO.setTimestamp(System.currentTimeMillis());
        sessionDTO.setUserId(UUID.randomUUID().toString());
        return sessionDTO;
    }

}