package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.InternalDangerImplementation;

import java.util.Collection;
import java.util.UUID;

@Setter
@RunWith(value = SpringRunner.class)
@Category(IntegrationImplementation.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class AdminEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Before
    public void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(this.authenticationEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
    }

    @Test
    public void testCloseAllSessions() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final int closeSessionsResponse = this.adminEndpoint.closedAllSessions(openSessionResponse);
        Assert.assertNotEquals(0, closeSessionsResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetAllSessions() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final Collection<SessionDTO> sessionsResponse = this.adminEndpoint.getAllSessions(openSessionResponse);
        Assert.assertNotNull(sessionsResponse);
        Assert.assertNotEquals(0, sessionsResponse.size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testSignUpUserWithUserRole() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.adminEndpoint.signUpUserWithUserRole(
                openSessionResponse, login, password, UserRole.USER
        );
        Assert.assertNotNull(addUserResponse);
        Assert.assertEquals(login, addUserResponse.getLogin());
        final int deleteUserResponse = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDeleteUserById() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final int deleteUserResponse = this.adminEndpoint.deleteUserById(openSessionResponse, addUserResponse.getId());
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testDeleteUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final int deleteUserResponse = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @Ignore("Делает работу остальных тестов некорректной")
    @Category(InternalDangerImplementation.class)
    public void testDeleteAllUsers() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        final int deletedUsersResponse = this.adminEndpoint.deleteAllUsers(openSessionResponse);
        Assert.assertEquals(1, deletedUsersResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testLockUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO lockUserResponse = this.adminEndpoint.lockUserByLogin(openSessionResponse, login);
        Assert.assertNotNull(lockUserResponse);
        Assert.assertEquals(addUserResponse.getId(), lockUserResponse.getId());
        Assert.assertEquals(login, lockUserResponse.getLogin());
        final int deleteUserResponse = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testUnlockUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);
        @Nullable final UserLimitedDTO lockUserResponse = this.adminEndpoint.lockUserByLogin(openSessionResponse, login);
        Assert.assertNotNull(lockUserResponse);

        @Nullable final UserLimitedDTO unlockUserResponse = this.adminEndpoint.unlockUserByLogin(openSessionResponse, login);
        Assert.assertNotNull(unlockUserResponse);
        Assert.assertEquals(addUserResponse.getId(), unlockUserResponse.getId());
        Assert.assertEquals(login, unlockUserResponse.getLogin());
        final int deleteUserResponse = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetAllUsers() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final Collection<UserLimitedDTO> usersResponse = adminEndpoint.getAllUsers(openSessionResponse);
        Assert.assertNotNull(usersResponse);
        Assert.assertNotEquals(0, usersResponse.size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetAllUsersTasks() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final Collection<TaskDTO> userTasksResponse = this.adminEndpoint.getAllUsersTasks(openSessionResponse);
        Assert.assertNotNull(userTasksResponse);
        Assert.assertNotEquals(0, userTasksResponse.size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetAllUsersProjects() {
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final Collection<ProjectDTO> userProjectsResponse = this.adminEndpoint.getAllUsersProjects(openSessionResponse);
        Assert.assertNotNull(userProjectsResponse);
        Assert.assertNotEquals(0, userProjectsResponse.size());
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetUserById() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO userResponse = this.adminEndpoint.getUserById(openSessionResponse, addUserResponse.getId());
        Assert.assertNotNull(userResponse);
        Assert.assertEquals(addUserResponse.getId(), userResponse.getId());
        Assert.assertEquals(login, userResponse.getLogin());
        final int deleteUserResponse = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testGetUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO userResponse = this.adminEndpoint.getUserByLogin(openSessionResponse, addUserResponse.getLogin());
        Assert.assertNotNull(userResponse);
        Assert.assertEquals(addUserResponse.getId(), userResponse.getId());
        Assert.assertEquals(login, userResponse.getLogin());
        final int deleteUserResponse = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testEditProfileById() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO editUserResponse = this.adminEndpoint.editProfileById(
                openSessionResponse, addUserResponse.getId(), "newFirstName"
        );
        Assert.assertNotNull(editUserResponse);
        Assert.assertEquals(addUserResponse.getId(), editUserResponse.getId());
        Assert.assertEquals(login, editUserResponse.getLogin());
        Assert.assertEquals("newFirstName", editUserResponse.getFirstName());
        final int deleteUserResponse = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    public void testUpdatePasswordById() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = this.authenticationEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = this.authenticationEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO updateUserResponse = this.adminEndpoint.updatePasswordById(
                openSessionResponse, addUserResponse.getId(), "newPassword"
        );
        Assert.assertNotNull(updateUserResponse);
        Assert.assertEquals(addUserResponse.getId(), updateUserResponse.getId());
        Assert.assertEquals(login, updateUserResponse.getLogin());
        final int deleteUserResponse = this.adminEndpoint.deleteUserByLogin(openSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeSessionResponse = this.authenticationEndpoint.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

}