package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.Collection;

@Setter
@RunWith(SpringRunner.class)
@Category(IntegrationImplementation.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public final class CommandServiceTest {

    @NotNull
    @Autowired
    private ICommandService commandService;

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.commandService);
    }

    @Test
    public void testGetAllCommands() {
        @Nullable final Collection<String> allCommands = this.commandService.getAllCommands();
        Assert.assertNotNull(allCommands);
    }

    @Test
    public void testGetAllTerminalCommands() {
        @Nullable final Collection<String> allTerminalCommands = this.commandService.getAllTerminalCommands();
        Assert.assertNotNull(allTerminalCommands);
    }

    @Test
    public void testAllArgumentCommands() {
        @Nullable final Collection<String> allArgumentCommands = this.commandService.getAllArgumentCommands();
        Assert.assertNotNull(allArgumentCommands);
    }

}