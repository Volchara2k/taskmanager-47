package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;

@Setter
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class DomainServiceTest {

    @NotNull
    @Autowired
    private IConfigurationService configService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IUserTaskService userTaskService;

    @NotNull
    @Autowired
    private IUserProjectService userProjectService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.userTaskService);
        Assert.assertNotNull(this.userProjectService);
        Assert.assertNotNull(this.domainService);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataExport() {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        Assert.assertNotNull(domainDTO);
        @NotNull final DomainDTO exportDomain = this.domainService.dataExport(domainDTO);
        Assert.assertNotNull(exportDomain);
        Assert.assertEquals(this.userService.exportRecords().size(), domainDTO.getUsers().size());
        Assert.assertEquals(this.userProjectService.exportRecords().size(), domainDTO.getProjects().size());
        Assert.assertEquals(this.userTaskService.exportRecords().size(), domainDTO.getTasks().size());
    }

    @Test
    @Ignore
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataImport() {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        Assert.assertNotNull(domainDTO);
        @NotNull final DomainDTO exportDomain = this.domainService.dataExport(domainDTO);
        Assert.assertNotNull(exportDomain);
        @Nullable final DomainDTO importDomain = this.domainService.dataImport(domainDTO);
        Assert.assertNotNull(importDomain);
        Assert.assertEquals(importDomain.getUsers().size(), this.userService.exportRecords().size());
        Assert.assertEquals(importDomain.getProjects().size(), this.userProjectService.exportRecords().size());
        Assert.assertEquals(importDomain.getTasks().size(), this.userTaskService.exportRecords().size());
    }

}