package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.util.FileUtil;

import java.io.File;

@Setter
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class DataInterChangeServiceTest {

    @NotNull
    @Autowired
    private IConfigurationService configurationService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IUserTaskService userTaskService;

    @NotNull
    @Autowired
    private IUserProjectService userProjectService;

    @NotNull
    @Autowired
    private IDataInterChangeService dataInterChangeService;

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.configurationService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.userTaskService);
        Assert.assertNotNull(this.userProjectService);
        Assert.assertNotNull(this.dataInterChangeService);
    }

    @After
    public void clearTempFilesAfter() {
        FileUtil.delete(this.configurationService.getBase64Pathname());
        FileUtil.delete(this.configurationService.getBinPathname());
        FileUtil.delete(this.configurationService.getBase64Pathname());
        FileUtil.delete(this.configurationService.getJsonPathname());
        FileUtil.delete(this.configurationService.getXmlPathname());
        FileUtil.delete(this.configurationService.getYamlPathname());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataBinClear() {
        @NotNull final File file = FileUtil.create(this.configurationService.getBinPathname());
        Assert.assertNotNull(file);
        final boolean isBinClear = this.dataInterChangeService.dataBinClear();
        Assert.assertTrue(isBinClear);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataBase64Clear() {
        @NotNull final File file = FileUtil.create(this.configurationService.getBase64Pathname());
        Assert.assertNotNull(file);
        final boolean isBase64Clear = this.dataInterChangeService.dataBase64Clear();
        Assert.assertTrue(isBase64Clear);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataJsonClear() {
        @NotNull final File file = FileUtil.create(this.configurationService.getJsonPathname());
        Assert.assertNotNull(file);
        final boolean isJsonClear = this.dataInterChangeService.dataJsonClear();
        Assert.assertTrue(isJsonClear);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataXmlClear() {
        @NotNull final File file = FileUtil.create(this.configurationService.getXmlPathname());
        Assert.assertNotNull(file);
        final boolean isXmlClear = this.dataInterChangeService.dataXmlClear();
        Assert.assertTrue(isXmlClear);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataYamlClear() {
        @NotNull final File file = FileUtil.create(this.configurationService.getYamlPathname());
        Assert.assertNotNull(file);
        final boolean isYamlClear = this.dataInterChangeService.dataYamlClear();
        Assert.assertTrue(isYamlClear);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testExportDataBin() {
        @NotNull final DomainDTO domain = this.dataInterChangeService.exportDataBin();
        Assert.assertNotNull(domain);
        Assert.assertNotEquals(0, domain.getUsers().size());
        Assert.assertNotEquals(0, domain.getTasks().size());
        Assert.assertNotEquals(0, domain.getProjects().size());
        Assert.assertEquals(this.userService.exportRecords().size(), domain.getUsers().size());
        Assert.assertEquals(this.userProjectService.exportRecords().size(), domain.getProjects().size());
        Assert.assertEquals(this.userTaskService.exportRecords().size(), domain.getTasks().size());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testExportDataBase64() {
        @NotNull final DomainDTO domain = this.dataInterChangeService.exportDataBase64();
        Assert.assertNotNull(domain);
        Assert.assertNotEquals(0, domain.getUsers().size());
        Assert.assertNotEquals(0, domain.getTasks().size());
        Assert.assertNotEquals(0, domain.getProjects().size());
        Assert.assertEquals(this.userService.exportRecords().size(), domain.getUsers().size());
        Assert.assertEquals(this.userProjectService.exportRecords().size(), domain.getProjects().size());
        Assert.assertEquals(this.userTaskService.exportRecords().size(), domain.getTasks().size());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testExportDataJson() {
        @NotNull final DomainDTO domain = this.dataInterChangeService.exportDataJson();
        Assert.assertNotNull(domain);
        Assert.assertNotEquals(0, domain.getUsers().size());
        Assert.assertNotEquals(0, domain.getTasks().size());
        Assert.assertNotEquals(0, domain.getProjects().size());
        Assert.assertEquals(this.userService.exportRecords().size(), domain.getUsers().size());
        Assert.assertEquals(this.userProjectService.exportRecords().size(), domain.getProjects().size());
        Assert.assertEquals(this.userTaskService.exportRecords().size(), domain.getTasks().size());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testExportDataXml() {
        @NotNull final DomainDTO domain = this.dataInterChangeService.exportDataXml();
        Assert.assertNotEquals(0, domain.getUsers().size());
        Assert.assertNotEquals(0, domain.getTasks().size());
        Assert.assertNotEquals(0, domain.getProjects().size());
        Assert.assertEquals(this.userService.exportRecords().size(), domain.getUsers().size());
        Assert.assertEquals(this.userProjectService.exportRecords().size(), domain.getProjects().size());
        Assert.assertEquals(this.userTaskService.exportRecords().size(), domain.getTasks().size());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testExportDataYaml() {
        @NotNull final DomainDTO domain = this.dataInterChangeService.exportDataYaml();
        Assert.assertNotNull(domain);
        Assert.assertNotEquals(0, domain.getUsers().size());
        Assert.assertNotEquals(0, domain.getTasks().size());
        Assert.assertNotEquals(0, domain.getProjects().size());
        Assert.assertEquals(this.userService.exportRecords().size(), domain.getUsers().size());
        Assert.assertEquals(this.userProjectService.exportRecords().size(), domain.getProjects().size());
        Assert.assertEquals(this.userTaskService.exportRecords().size(), domain.getTasks().size());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testImportDataBin() {
        @NotNull final DomainDTO exportDomain = this.dataInterChangeService.exportDataBin();
        Assert.assertNotNull(exportDomain);
        @NotNull final DomainDTO importDomain = this.dataInterChangeService.importDataBin();
        Assert.assertNotNull(importDomain);
        Assert.assertNotEquals(0, importDomain.getUsers().size());
        Assert.assertNotEquals(0, importDomain.getTasks().size());
        Assert.assertNotEquals(0, importDomain.getProjects().size());
        Assert.assertEquals(importDomain.getUsers().size(), this.userService.exportRecords().size());
        Assert.assertEquals(importDomain.getProjects().size(), this.userProjectService.exportRecords().size());
        Assert.assertEquals(importDomain.getTasks().size(), this.userTaskService.exportRecords().size());
    }

//    @Test
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    public void testImportDataBase64() {
//        @NotNull final DomainDTO exportDomain = this.dataInterChangeService.exportDataBase64();
//        Assert.assertNotNull(exportDomain);
//        @NotNull final DomainDTO domain = this.dataInterChangeService.importDataBase64();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().size(), this.userService.exportRecords().size());
//        Assert.assertEquals(domain.getProjects().size(), this.userProjectService.exportRecords().size());
//        Assert.assertEquals(domain.getTasks().size(), this.userTaskService.exportRecords().size());
//    }
//
//    @Test
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    public void testImportDataJson() {
//        @NotNull final DomainDTO exportDomain = this.dataInterChangeService.exportDataJson();
//        Assert.assertNotNull(exportDomain);
//        @NotNull final DomainDTO domain = this.dataInterChangeService.importDataJson();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().size(), this.userService.exportRecords().size());
//        Assert.assertEquals(domain.getProjects().size(), this.userProjectService.exportRecords().size());
//        Assert.assertEquals(domain.getTasks().size(), this.userTaskService.exportRecords().size());
//    }

//    @Test
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    public void testImportDataXml() {
//        @NotNull final DomainDTO exportDomain = this.dataInterChangeService.exportDataXml();
//        Assert.assertNotNull(exportDomain);
//        @NotNull final DomainDTO domain = this.dataInterChangeService.importDataXml();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().size(), this.userService.exportRecords().size());
//        Assert.assertEquals(domain.getProjects().size(), this.userProjectService.exportRecords().size());
//        Assert.assertEquals(domain.getTasks().size(), this.userTaskService.exportRecords().size());
//    }
//
//    @Test
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    public void testImportDataYaml() {
//        @NotNull final DomainDTO exportDomain = this.dataInterChangeService.exportDataYaml();
//        Assert.assertNotNull(exportDomain);
//        @NotNull final DomainDTO domain = this.dataInterChangeService.importDataYaml();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().size(), this.userService.exportRecords().size());
//        Assert.assertEquals(domain.getProjects().size(), this.userProjectService.exportRecords().size());
//        Assert.assertEquals(domain.getTasks().size(), this.userTaskService.exportRecords().size());
//    }

}