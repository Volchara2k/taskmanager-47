package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Session;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;

@Setter
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class SessionServiceTest {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @Before
    public void assertMainComponentsNotNullBefore() {
        Assert.assertNotNull(this.sessionService);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testSetSignature() {
        @NotNull final Session session = new Session();
        @NotNull final Session signatureSession = this.sessionService.addSignature(session);
        Assert.assertNotNull(signatureSession);
        Assert.assertEquals(session.getId(), signatureSession.getId());
        Assert.assertEquals(session.getUserId(), signatureSession.getUserId());
        Assert.assertEquals(session.getTimestamp(), signatureSession.getTimestamp());
        Assert.assertEquals(session.getSignature(), signatureSession.getSignature());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testOpenSession() {
        @NotNull final Session openSession = this.sessionService.openSession("test", "test");
        Assert.assertNotNull(openSession);
        @Nullable final Session session = this.sessionService.getRecordById(openSession.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(openSession.getId(), session.getId());
        Assert.assertEquals(openSession.getUserId(), session.getUserId());
        Assert.assertEquals(openSession.getTimestamp(), session.getTimestamp());
        Assert.assertEquals(openSession.getSignature(), session.getSignature());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testCloseSession() {
        @NotNull final Session openSession = this.sessionService.openSession("test", "test");
        Assert.assertNotNull(openSession);
        @Nullable final Session closeSession = this.sessionService.closeSession(openSession);
        Assert.assertNotNull(closeSession);
        Assert.assertEquals(openSession.getId(), closeSession.getId());
        Assert.assertEquals(openSession.getUserId(), closeSession.getUserId());
        Assert.assertEquals(openSession.getTimestamp(), closeSession.getTimestamp());
        Assert.assertEquals(openSession.getSignature(), closeSession.getSignature());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testCloseAllSessions() {
        @NotNull final Session openSession = this.sessionService.openSession("test", "test");
        Assert.assertNotNull(openSession);
        final int isClosedSession = this.sessionService.closeAllSessions(openSession);
        Assert.assertNotEquals(0, isClosedSession);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSessionByUserId() {
        @NotNull final Session openSession = this.sessionService.openSession("test", "test");
        Assert.assertNotNull(openSession);
        @Nullable final Session session = this.sessionService.getRecordById(openSession.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(openSession.getId(), session.getId());
        Assert.assertEquals(openSession.getUserId(), session.getUserId());
        Assert.assertEquals(openSession.getTimestamp(), session.getTimestamp());
        Assert.assertEquals(openSession.getSignature(), session.getSignature());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDeleteSessionByUserId() {
        @NotNull final Session openSession = this.sessionService.openSession("test", "test");
        Assert.assertNotNull(openSession);
        final int deleteSession = this.sessionService.deleteRecordById(openSession.getId());
        Assert.assertEquals(1, deleteSession);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testValidateUserData() {
        @NotNull final User validate = this.sessionService.validateUserData("test", "test");
        Assert.assertNotNull(validate);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testVerifyValidUserData() {
        @NotNull final UserDataValidState userDataValidState = this.sessionService.verifyValidUserData("admin", "admin");
        Assert.assertNotNull(userDataValidState);
        Assert.assertEquals(UserDataValidState.SUCCESS, userDataValidState);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testValidateSession() {
        @NotNull final Session openSession = this.sessionService.openSession("test", "test");
        Assert.assertNotNull(openSession);

        @NotNull final Session validate = this.sessionService.validateSession(openSession);
        Assert.assertNotNull(validate);
        Assert.assertEquals(openSession.getId(), validate.getId());
        Assert.assertEquals(openSession.getUserId(), validate.getUserId());
        Assert.assertEquals(openSession.getTimestamp(), validate.getTimestamp());
        Assert.assertEquals(openSession.getSignature(), validate.getSignature());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testValidateSessionWithRole() {
        @NotNull final Session open = this.sessionService.openSession("test", "test");
        Assert.assertNotNull(open);

        @NotNull final Session validate = this.sessionService.validateSession(open, UserRoleType.USER);
        Assert.assertNotNull(validate);
        Assert.assertEquals(open.getId(), validate.getId());
        Assert.assertEquals(open.getUserId(), validate.getUserId());
        Assert.assertEquals(open.getTimestamp(), validate.getTimestamp());
        Assert.assertEquals(open.getSignature(), validate.getSignature());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testVerifyValidSessionState() {
        @NotNull final Session open = this.sessionService.openSession("test", "test");
        Assert.assertNotNull(open);
        @NotNull final SessionValidState verifyValidSessionState = this.sessionService.verifyValidSessionState(open);
        Assert.assertNotNull(verifyValidSessionState);
        Assert.assertEquals(SessionValidState.SUCCESS, verifyValidSessionState);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testVerifyValidPermissionState() {
        @NotNull final Session openSession = this.sessionService.openSession("test", "test");
        Assert.assertNotNull(openSession);

        @NotNull final PermissionValidState permissionValidState =
                this.sessionService.verifyValidPermissionState(openSession, UserRoleType.USER);
        Assert.assertNotNull(permissionValidState);
        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
    }

//    @Test(expected = AccessFailureException.class)
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeSetSignature() {
//        this.sessionService.addSignature(null);
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeOpenSession() {
//        @NotNull final InvalidLoginException loginThrown = assertThrows(
//                InvalidLoginException.class,
//                () -> this.sessionService.openSession("   ", "password")
//        );
//        Assert.assertNotNull(loginThrown);
//        Assert.assertNotNull(loginThrown.getMessage());
//
//        @NotNull final String tempLogin = UUID.randomUUID().toString();
//        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
//                InvalidPasswordException.class,
//                () -> this.sessionService.openSession(tempLogin, "  ")
//        );
//        Assert.assertNotNull(passwordThrown);
//        Assert.assertNotNull(passwordThrown.getMessage());
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeValidateUserData() {
//        @NotNull final InvalidLoginException loginThrown = assertThrows(
//                InvalidLoginException.class,
//                () -> this.sessionService.verifyValidUserData(null, "password")
//        );
//        Assert.assertNotNull(loginThrown);
//        Assert.assertNotNull(loginThrown.getMessage());
//
//        @NotNull final String tempLogin = UUID.randomUUID().toString();
//        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
//                InvalidPasswordException.class,
//                () -> this.sessionService.verifyValidUserData(tempLogin, "")
//        );
//        Assert.assertNotNull(passwordThrown);
//        Assert.assertNotNull(passwordThrown.getMessage());
//    }
//
//    @Test(expected = AccessFailureException.class)
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeValidateSession() {
//        this.sessionService.validateSession(null);
//    }
//}

//    @Test
//    @TestCaseName("Run testNegativeValidateSessionWithRole for validateSession({1}, role)")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testNegativeValidateSessionWithRole(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final Session addRecord = SESSION_SERVICE.persist(session);
//        Assert.assertNotNull(addRecord);
//
//        @NotNull final AccessFailureException thrownValidSessionState = assertThrows(
//                AccessFailureException.class,
//                () -> SESSION_SERVICE.validateSession(session, UserRole.UNKNOWN)
//        );
//        Assert.assertNotNull(thrownValidSessionState);
//        Assert.assertNotNull(thrownValidSessionState.getMessage());
//
//        @NotNull final Session signatureSession = SESSION_SERVICE.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//
//        @NotNull final AccessFailureException thrownValidPermissionState = assertThrows(
//                AccessFailureException.class,
//                () -> SESSION_SERVICE.validateSession(signatureSession, UserRole.UNKNOWN)
//        );
//        Assert.assertNotNull(thrownValidPermissionState);
//        Assert.assertNotNull(thrownValidPermissionState.getMessage());
//        //final boolean deleteUser = USER_SERVICE.deletedRecord(addUser);
//        //Assert.assertTrue(deleteUser);
//  }

}