package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Task;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

@Setter
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class UserTaskServiceTest {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IUserTaskService userTaskService;

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.userTaskService);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testAdd() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);

        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);
        Assert.assertEquals(title, addTask.getTitle());
        Assert.assertEquals(description, addTask.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testUpdateByIndex() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);

        @NotNull final String newData = UUID.randomUUID().toString();
        Assert.assertNotNull(newData);
        @Nullable final Task updateTask = this.userTaskService.updateUserOwnerByIndex(user.getId(), 0, newData, newData);
        Assert.assertNotNull(updateTask);
        Assert.assertEquals(newData, updateTask.getTitle());
        Assert.assertEquals(newData, updateTask.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testUpdateById() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);

        @NotNull final String newData = UUID.randomUUID().toString();
        Assert.assertNotNull(newData);
        @Nullable final Task updateTask = this.userTaskService.updateUserOwnerById(user.getId(), addTask.getId(), newData, newData);
        Assert.assertNotNull(updateTask);
        Assert.assertEquals(addTask.getId(), updateTask.getId());
        Assert.assertEquals(addTask.getUserId(), updateTask.getUserId());
        Assert.assertEquals(newData, updateTask.getTitle());
        Assert.assertEquals(newData, updateTask.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDeleteByIndex() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);

        final int deleteTask = this.userTaskService.deleteUserOwnerByIndex(user.getId(), 0);
        Assert.assertEquals(1, deleteTask);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDeleteById() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);

        final int deleteTask = this.userTaskService.deleteUserOwnerById(user.getId(), addTask.getId());
        Assert.assertEquals(1, deleteTask);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDeleteByTitle() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);

        final int deleteTask = this.userTaskService.deleteUserOwnerByTitle(user.getId(), addTask.getTitle());
        Assert.assertEquals(1, deleteTask);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDeleteAll() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);

        final int deleteTasks = this.userTaskService.deleteUserOwnerAll(user.getId());
        Assert.assertNotEquals(0, deleteTasks);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetByIndex() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);

        @Nullable final Task task = this.userTaskService.getUserOwnerByIndex(user.getId(), 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(addTask.getId(), task.getId());
        Assert.assertEquals(addTask.getUserId(), task.getUserId());
        Assert.assertEquals(addTask.getTitle(), task.getTitle());
        Assert.assertEquals(addTask.getDescription(), task.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetById() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);

        @Nullable final Task task = this.userTaskService.getUserOwnerById(user.getId(), addTask.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(addTask.getId(), task.getId());
        Assert.assertEquals(addTask.getUserId(), task.getUserId());
        Assert.assertEquals(addTask.getTitle(), task.getTitle());
        Assert.assertEquals(addTask.getDescription(), task.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetByTitle() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);

        @Nullable final Task task = this.userTaskService.getUserOwnerByTitle(user.getId(), addTask.getTitle());
        Assert.assertNotNull(task);
        Assert.assertEquals(addTask.getId(), task.getId());
        Assert.assertEquals(addTask.getUserId(), task.getUserId());
        Assert.assertEquals(addTask.getTitle(), task.getTitle());
        Assert.assertEquals(addTask.getDescription(), task.getDescription());
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetAll() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.userTaskService.addUserOwner(
                user.getId(), title, description
        );
        Assert.assertNotNull(addTask);

        @Nullable final Collection<Task> tasks = this.userTaskService.getUserOwnerAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertNotEquals(0, tasks.size());
        final boolean isUserTasks = tasks.stream().allMatch(entity -> user.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testInitialDemoData() {
        @Nullable final User user = this.userService.getUserByLogin("test");
        Assert.assertNotNull(user);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Collection<User> users = Collections.singletonList(user);
        Assert.assertNotNull(users);

        @NotNull final Collection<Task> initTasks = this.userTaskService.initialUserOwner(users);
        Assert.assertNotNull(initTasks);
    }

//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeAdd() {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userTaskService.addUserOwner("", "title", "description")
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> userTaskService.addUserOwner(userIdTemp, " ", "description")
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//
//        @NotNull final String titleTemp = DemoDataConst.DEMO_TITLE_1ST;
//        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
//                InvalidDescriptionException.class,
//                () -> userTaskService.addUserOwner(userIdTemp, titleTemp, null)
//        );
//        Assert.assertNotNull(descriptionThrown);
//        Assert.assertNotNull(descriptionThrown.getMessage());
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeUpdateByIndex() {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userTaskService.updateUserOwnerByIndex(null, 1, "title", "description")
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final IllegalIndexException indexThrown = assertThrows(
//                IllegalIndexException.class,
//                () -> userTaskService.updateUserOwnerByIndex(userIdTemp, -1, "title", "description")
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//
//        @NotNull final Integer indexTemp = 1;
//        Assert.assertNotNull(indexTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> userTaskService.updateUserOwnerByIndex(userIdTemp, indexTemp, "", "description")
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//
//        @NotNull final String titleTemp = DemoDataConst.DEMO_TITLE_1ST;
//        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
//                InvalidDescriptionException.class,
//                () -> userTaskService.updateUserOwnerByIndex(userIdTemp, indexTemp, titleTemp, "    ")
//        );
//        Assert.assertNotNull(descriptionThrown);
//        Assert.assertNotNull(descriptionThrown.getMessage());
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeUpdateById() {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userTaskService.updateUserOwnerById("      ", "id", "title", "description")
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidIdException indexThrown = assertThrows(
//                InvalidIdException.class,
//                () -> userTaskService.updateUserOwnerById(userIdTemp, null, "title", "description")
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//
//        @NotNull final String idTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(idTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> userTaskService.updateUserOwnerById(userIdTemp, idTemp, "", "description")
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//
//        @NotNull final String titleTemp = DemoDataConst.DEMO_TITLE_1ST;
//        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
//                InvalidDescriptionException.class,
//                () -> userTaskService.updateUserOwnerById(userIdTemp, idTemp, titleTemp, "     ")
//        );
//        Assert.assertNotNull(descriptionThrown);
//        Assert.assertNotNull(descriptionThrown.getMessage());
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeDeleteByIndex() {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userTaskService.deleteUserOwnerByIndex(" ", 1)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final IllegalIndexException indexThrown = assertThrows(
//                IllegalIndexException.class,
//                () -> userTaskService.deleteUserOwnerByIndex(userIdTemp, -10)
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeDeleteById() {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userTaskService.deleteUserOwnerById("  ", "id")
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidIdException idThrown = assertThrows(
//                InvalidIdException.class,
//                () -> userTaskService.deleteUserOwnerById(userIdTemp, null)
//        );
//        Assert.assertNotNull(idThrown);
//        Assert.assertNotNull(idThrown.getMessage());
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeDeleteByTitle() {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.userTaskService.deleteUserOwnerById(" ", "id")
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> this.userTaskService.deleteUserOwnerByTitle(userIdTemp, "      ")
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeDeleteAll() {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userTaskService.deleteUserOwnerAll("     ")
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeGetByIndex() {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userTaskService.getUserOwnerByIndex(null, 100)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final IllegalIndexException indexThrown = assertThrows(
//                IllegalIndexException.class,
//                () -> userTaskService.getUserOwnerByIndex(userIdTemp, -100)
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeGetById() {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userTaskService.getUserOwnerById(null, "id")
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidIdException idThrown = assertThrows(
//                InvalidIdException.class,
//                () -> userTaskService.getUserOwnerById(userIdTemp, null)
//        );
//        Assert.assertNotNull(idThrown);
//        Assert.assertNotNull(idThrown.getMessage());
//    }
//
//    @Test
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeGetByTitle() {
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> userTaskService.getUserOwnerByTitle("       ", "title")
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> this.userTaskService.getUserOwnerByTitle(userIdTemp, "")
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//    }
//
//    @Test(expected = InvalidUserIdException.class)
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeGetAll() {
//        this.userTaskService.getUserOwnerAll(null);
//    }
//
//    @Test(expected = InvalidUserException.class)
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeInitialDemoData() {
//        this.userTaskService.initialUserOwner(null);
//    }

}