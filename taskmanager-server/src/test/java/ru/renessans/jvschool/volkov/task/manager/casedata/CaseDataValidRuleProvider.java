package ru.renessans.jvschool.volkov.task.manager.casedata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@SuppressWarnings("unused")
public final class CaseDataValidRuleProvider {

    public Object[] invalidStringsCaseData() {
        return new Object[]{
                new Object[]{true, null},
                new Object[]{true, "    "},
                new Object[]{true, ""}
        };
    }

    public Object[] invalidIntegersCaseData() {
        return new Object[]{
                new Object[]{true, null},
                new Object[]{true, -1},
                new Object[]{true, -10},
                new Object[]{true, -23 * 2},
                new Object[]{true, -1981}
        };
    }

    public Object[] invalidLongsCaseData() {
        return new Object[]{
                new Object[]{true, null},
                new Object[]{true, -1L},
                new Object[]{true, -10L},
                new Object[]{true, -23L * 2L},
                new Object[]{true, -1981L}
        };
    }

    public Object[] invalidCollectionsCaseData() {
        return new Object[]{
                new Object[]{true, null},
                new Object[]{
                        true,
                        Arrays.asList(
                                null,
                                null,
                                null
                        )
                },
                new Object[]{true, Collections.emptyList()},
                new Object[]{true, new ArrayList<>()}
        };
    }

    public Object[] validStringsCaseData() {
        return new Object[]{
                new Object[]{false, "null"},
                new Object[]{false, "    1"},
                new Object[]{false, "dwa       dwa"},
                new Object[]{false, "1      "},
                new Object[]{false, "#!@  "}
        };
    }

    public Object[] validIntegersCaseData() {
        return new Object[]{
                new Object[]{false, 0},
                new Object[]{false, 1},
                new Object[]{false, 23},
                new Object[]{false, -1 * -1},
                new Object[]{false, 1981}
        };
    }

    public Object[] validLongsCaseData() {
        return new Object[]{
                new Object[]{false, 1L},
                new Object[]{false, 23L},
                new Object[]{false, -1L * -1L},
                new Object[]{false, 1981L}
        };
    }

    public Object[] validCollectionsCaseData() {
        return new Object[]{
                new Object[]{
                        false,
                        Arrays.asList(
                                "",
                                "",
                                ""
                        )
                },
                new Object[]{
                        false,
                        Arrays.asList(
                                "null",
                                "not null",
                                ""
                        )
                },
                new Object[]{
                        false,
                        Arrays.asList(
                                null,
                                "wolf",
                                null
                        )
                },
                new Object[]{
                        false,
                        Arrays.asList(
                                "string",
                                "data    ",
                                ", c..ca"
                        )
                }
        };
    }

}