package ru.renessans.jvschool.volkov.task.manager.util;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataValidRuleProvider;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

import java.util.Collection;

@RunWith(value = JUnitParamsRunner.class)
public final class ValidRuleUtilTest {

    @Test
    @TestCaseName("Run testIsNullOrEmptyString: {0} for isNullOrEmpty(\"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "invalidStringsCaseData"
    )
    public void testIsNullOrEmptyString(
            final boolean result,
            @Nullable final String validator
    ) {
        final boolean isNullOrEmptyString = ValidRuleUtil.isNullOrEmpty(validator);
        Assert.assertEquals(result, isNullOrEmptyString);
    }

    @Test
    @TestCaseName("Run testIsNullOrEmptyInteger: {0} for isNullOrEmpty({1})")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "invalidIntegersCaseData"
    )
    public void testIsNullOrEmptyInteger(
            final boolean result,
            @Nullable final Integer validator
    ) {
        final boolean isNullOrEmptyInteger = ValidRuleUtil.isNullOrEmpty(validator);
        Assert.assertEquals(result, isNullOrEmptyInteger);
    }

    @Test
    @TestCaseName("Run testIsNullOrEmptyLong: {0} for isNullOrEmpty({1})")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "invalidLongsCaseData"
    )
    public void testIsNullOrEmptyLong(
            final boolean result,
            @Nullable final Long validator
    ) {
        final boolean isNullOrEmptyLong = ValidRuleUtil.isNullOrEmpty(validator);
        Assert.assertEquals(result, isNullOrEmptyLong);
    }

    @Test
    @TestCaseName("Run testIsNullOrEmptyCollection: {0} for isNullOrEmpty({1})")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "invalidCollectionsCaseData"
    )
    public void testIsNullOrEmptyCollection(
            final boolean result,
            @Nullable final Collection<Object> validator
    ) {
        final boolean isNotNullOrEmptyLong = ValidRuleUtil.isNullOrEmpty(validator);
        Assert.assertEquals(result, isNotNullOrEmptyLong);
    }

    @Test
    @TestCaseName("Run testIsNotNullOrEmptyString: {0} for isNullOrEmpty(\"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "validStringsCaseData"
    )
    public void testIsNotNullOrEmptyString(
            final boolean result,
            @NotNull final String validator
    ) {
        Assert.assertNotNull(validator);
        final boolean isNotNullOrEmptyString = ValidRuleUtil.isNullOrEmpty(validator);
        Assert.assertEquals(result, isNotNullOrEmptyString);
    }

    @Test
    @TestCaseName("Run testIsNotNullOrEmptyInteger: {0} for isNullOrEmpty({1})")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "validIntegersCaseData"
    )
    public void testIsNotNullOrEmptyInteger(
            final boolean result,
            @NotNull final Integer validator
    ) {
        Assert.assertNotNull(validator);
        final boolean isNotNullOrEmptyInteger = ValidRuleUtil.isNullOrEmpty(validator);
        Assert.assertEquals(result, isNotNullOrEmptyInteger);
    }

    @Test
    @TestCaseName("Run testIsNotNullOrEmptyLong: {0} for isNullOrEmpty({1})")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "validLongsCaseData"
    )
    public void testIsNotNullOrEmptyLong(
            final boolean result,
            @NotNull final Long validator
    ) {
        Assert.assertNotNull(validator);
        final boolean isNotNullOrEmptyLong = ValidRuleUtil.isNullOrEmpty(validator);
        Assert.assertEquals(result, isNotNullOrEmptyLong);
    }

    @Test
    @TestCaseName("Run testIsNotNullOrEmptyCollection: {0} for isNullOrEmpty({1})")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "validCollectionsCaseData"
    )
    public void testIsNotNullOrEmptyCollection(
            final boolean result,
            @NotNull final Collection<Object> validator
    ) {
        Assert.assertNotNull(validator);
        final boolean isNotNullOrEmptyLong = ValidRuleUtil.isNullOrEmpty(validator);
        Assert.assertEquals(result, isNotNullOrEmptyLong);
    }

}