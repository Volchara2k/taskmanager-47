package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.configuration.ApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;

@Setter
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public final class ConfigurationServiceTest {

    @NotNull
    @Autowired
    private IConfigurationService configurationService;

    @Before
    public void assertMainComponentsBefore() {
        Assert.assertNotNull(this.configurationService);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetServerHost() {
        @NotNull final String serverHost = this.configurationService.getServerHost();
        Assert.assertNotNull(serverHost);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetServerPort() {
        @NotNull final Integer serverPort = this.configurationService.getServerPort();
        Assert.assertNotNull(serverPort);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSessionSalt() {
        @NotNull final String sessionSalt = this.configurationService.getSessionSalt();
        Assert.assertNotNull(sessionSalt);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSessionCycle() {
        @NotNull final Integer sessionCycle = this.configurationService.getSessionCycle();
        Assert.assertNotNull(sessionCycle);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetBinPathname() {
        @NotNull final String binPathname = this.configurationService.getBinPathname();
        Assert.assertNotNull(binPathname);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetBase64Pathname() {
        @NotNull final String base64Pathname = this.configurationService.getBase64Pathname();
        Assert.assertNotNull(base64Pathname);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetJsonPathname() {
        @NotNull final String jsonPathname = this.configurationService.getJsonPathname();
        Assert.assertNotNull(jsonPathname);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetXmlPathname() {
        @NotNull final String xmlPathname = this.configurationService.getXmlPathname();
        Assert.assertNotNull(xmlPathname);
    }

    @Test
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetYamlPathname() {
        @NotNull final String yamlPathname = this.configurationService.getYamlPathname();
        Assert.assertNotNull(yamlPathname);
    }

}