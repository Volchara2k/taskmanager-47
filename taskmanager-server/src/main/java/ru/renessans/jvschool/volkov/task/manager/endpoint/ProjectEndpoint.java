package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.stream.Collectors;

@WebService
@Controller
@RequiredArgsConstructor
public final class ProjectEndpoint implements IProjectEndpoint {

    @NotNull
    private final IUserProjectService projectUserService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final ISessionAdapterService sessionAdapterService;

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @Override
    public ProjectDTO addProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @NotNull final Project project = this.projectUserService.addUserOwner(current.getUserId(), title, description);
        return this.projectAdapterService.toDTO(project);
    }

    @WebMethod
    @WebResult(name = "updatedProject", partName = "updatedProject")
    @Nullable
    @Override
    public ProjectDTO updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Project project = this.projectUserService.updateUserOwnerByIndex(current.getUserId(), index, title, description);
        return this.projectAdapterService.toDTO(project);
    }

    @WebMethod
    @WebResult(name = "updatedProject", partName = "updatedProject")
    @Nullable
    @Override
    public ProjectDTO updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Project project = this.projectUserService.updateUserOwnerById(current.getUserId(), id, title, description);
        return this.projectAdapterService.toDTO(project);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.projectUserService.deleteUserOwnerById(current.getUserId(), id);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.projectUserService.deleteUserOwnerByIndex(current.getUserId(), index);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteProjectByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.projectUserService.deleteUserOwnerByTitle(current.getUserId(), title);
    }

    @WebMethod
    @WebResult(name = "deletedProjects", partName = "deletedProjects")
    @Override
    public int deleteAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        return this.projectUserService.deleteUserOwnerAll(current.getUserId());
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @Override
    public ProjectDTO getProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Project project = this.projectUserService.getUserOwnerById(current.getUserId(), id);
        return this.projectAdapterService.toDTO(project);
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @Override
    public ProjectDTO getProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Project project = this.projectUserService.getUserOwnerByIndex(current.getUserId(), index);
        return this.projectAdapterService.toDTO(project);
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @Override
    public ProjectDTO getProjectByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        @Nullable final Project project = this.projectUserService.getUserOwnerByTitle(current.getUserId(), title);
        return this.projectAdapterService.toDTO(project);
    }

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    @Override
    public Collection<ProjectDTO> getAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        return this.projectUserService.getUserOwnerAll(current.getUserId())
                .stream()
                .map(this.projectAdapterService::toDTO)
                .collect(Collectors.toList());
    }

}