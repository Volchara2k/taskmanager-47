package ru.renessans.jvschool.volkov.task.manager.model.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Date;

@Getter
@Setter
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public final class TimeFrame {

    @NotNull
    @Column(
            nullable = false,
            updatable = false
    )
    private Date creationDate = new Date();

}