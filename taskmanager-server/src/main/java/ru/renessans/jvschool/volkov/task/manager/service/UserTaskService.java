package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalGetProjectException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Project;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Task;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserTaskRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Objects;

@Service
@Transactional
public final class UserTaskService extends AbstractUserOwnerService<Task> implements IUserTaskService {

    @NotNull
    private final IUserProjectService userProjectService;

    public UserTaskService(
            @NotNull final IUserTaskRepository userTaskRepository,
            @NotNull final IUserService userService,
            @NotNull final IUserProjectService userProjectService
    ) {
        super(userTaskRepository, userService);
        this.userProjectService = userProjectService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task addUserOwner(
            @Nullable final String userId,
            @Nullable final String projectTitle,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(projectTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();

        @Nullable final Project project = this.userProjectService.getUserOwnerByTitle(userId, projectTitle);
        if (Objects.isNull(project)) throw new IllegalGetProjectException();
        @NotNull final Task task = new Task(userId, title, description);
        task.setProject(project);

        return super.addUserOwner(task);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task addUserOwner(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();
        @NotNull final Task task = new Task(userId, title, description);
        return super.addUserOwner(task);
    }

}