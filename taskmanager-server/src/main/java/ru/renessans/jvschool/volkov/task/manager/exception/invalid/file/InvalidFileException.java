package ru.renessans.jvschool.volkov.task.manager.exception.invalid.file;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidFileException extends AbstractException {

    @NotNull
    private static final String EMPTY_FILE =
            "Ошибка! Параметр \"файл\" отсутствует!\n";

    public InvalidFileException() {
        super(EMPTY_FILE);
    }

}