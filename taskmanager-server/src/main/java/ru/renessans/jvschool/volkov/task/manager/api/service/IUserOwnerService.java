package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IService;
import ru.renessans.jvschool.volkov.task.manager.model.entity.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;

import java.util.Collection;

public interface IUserOwnerService<E extends AbstractModel> extends IService<E> {

    @NotNull
    E addUserOwner(
            @Nullable E value
    );

    @NotNull
    E addUserOwner(
            @Nullable String userId,
            @Nullable String title,
            @Nullable String description
    );

    @NotNull
    E updateUserOwnerByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String newTitle,
            @Nullable String newDescription
    );

    @NotNull
    E updateUserOwnerById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String newTitle,
            @Nullable String newDescription
    );

    int deleteUserOwnerByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    int deleteUserOwnerById(
            @Nullable String userId,
            @Nullable String id
    );

    int deleteUserOwnerByTitle(
            @Nullable String userId,
            @Nullable String title
    );

    int deleteUserOwnerAll(
            @Nullable String userId
    );

    @Nullable
    E getUserOwnerByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    E getUserOwnerById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    E getUserOwnerByTitle(
            @Nullable String userId,
            @Nullable String title
    );

    @NotNull
    Collection<E> getUserOwnerAll(
            @Nullable String userId
    );

    long countUserOwner(
            @Nullable String userId
    );

    @NotNull
    Collection<E> initialUserOwner(
            @Nullable Collection<User> users
    );

    @NotNull
    Collection<E> initialUserOwner();

}