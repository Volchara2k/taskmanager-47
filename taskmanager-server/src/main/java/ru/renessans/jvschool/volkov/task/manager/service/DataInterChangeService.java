package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDataInterChangeService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDomainService;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.util.DataMarshalizerUtil;
import ru.renessans.jvschool.volkov.task.manager.util.DataSerializerUtil;
import ru.renessans.jvschool.volkov.task.manager.util.FileUtil;

@Service
@RequiredArgsConstructor
public final class DataInterChangeService implements IDataInterChangeService {

    @NotNull
    private final IDomainService domainService;

    @NotNull
    private final IConfigurationService configurationService;

    @Override
    public boolean dataBinClear() {
        @NotNull final String pathname = this.configurationService.getBinPathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataBase64Clear() {
        @NotNull final String pathname = this.configurationService.getBase64Pathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataJsonClear() {
        @NotNull final String pathname = this.configurationService.getJsonPathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataXmlClear() {
        @NotNull final String pathname = this.configurationService.getXmlPathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataYamlClear() {
        @NotNull final String pathname = this.configurationService.getYamlPathname();
        return FileUtil.delete(pathname);
    }

    @NotNull
    @SneakyThrows
    @Override
    public DomainDTO exportDataBin() {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        this.domainService.dataExport(domainDTO);
        @NotNull final String pathname = this.configurationService.getBinPathname();
        DataSerializerUtil.writeToBin(domainDTO, pathname);
        return domainDTO;
    }

    @NotNull
    @SneakyThrows
    @Override
    public DomainDTO exportDataBase64() {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        this.domainService.dataExport(domainDTO);
        @NotNull final String pathname = this.configurationService.getBase64Pathname();
        DataSerializerUtil.writeToBase64(domainDTO, pathname);
        return domainDTO;
    }

    @NotNull
    @Override
    public DomainDTO exportDataJson() {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        this.domainService.dataExport(domainDTO);
        @NotNull final String pathname = this.configurationService.getJsonPathname();
        DataMarshalizerUtil.writeToJson(domainDTO, pathname);
        return domainDTO;
    }

    @NotNull
    @Override
    public DomainDTO exportDataXml() {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        this.domainService.dataExport(domainDTO);
        @NotNull final String pathname = this.configurationService.getXmlPathname();
        DataMarshalizerUtil.writeToXml(domainDTO, pathname);
        return domainDTO;
    }

    @NotNull
    @SneakyThrows
    @Override
    public DomainDTO exportDataYaml() {
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        this.domainService.dataExport(domainDTO);
        @NotNull final String pathname = this.configurationService.getYamlPathname();
        DataMarshalizerUtil.writeToYaml(domainDTO, pathname);
        return domainDTO;
    }

    @NotNull
    @SneakyThrows
    @Override
    public DomainDTO importDataBin() {
        @NotNull final String pathname = this.configurationService.getBinPathname();
        @NotNull final DomainDTO domainDTO = DataSerializerUtil.readFromBin(pathname, DomainDTO.class);
        return this.domainService.dataImport(domainDTO);
    }

    @NotNull
    @SneakyThrows
    @Override
    public DomainDTO importDataBase64() {
        @NotNull final String pathname = this.configurationService.getBase64Pathname();
        @NotNull final DomainDTO domainDTO = DataSerializerUtil.readFromBase64(pathname, DomainDTO.class);
        return this.domainService.dataImport(domainDTO);
    }

    @NotNull
    @Override
    public DomainDTO importDataJson() {
        @NotNull final String pathname = this.configurationService.getJsonPathname();
        @NotNull final DomainDTO domainDTO = DataMarshalizerUtil.readFromJson(pathname, DomainDTO.class);
        return this.domainService.dataImport(domainDTO);
    }

    @NotNull
    @Override
    public DomainDTO importDataXml() {
        @NotNull final String pathname = this.configurationService.getXmlPathname();
        @NotNull final DomainDTO domainDTO = DataMarshalizerUtil.readFromXml(pathname, DomainDTO.class);
        return this.domainService.dataImport(domainDTO);
    }

    @NotNull
    @Override
    public DomainDTO importDataYaml() {
        @NotNull final String pathname = this.configurationService.getYamlPathname();
        @NotNull final DomainDTO domainDTO = DataMarshalizerUtil.readFromYaml(pathname, DomainDTO.class);
        return this.domainService.dataImport(domainDTO);
    }

}