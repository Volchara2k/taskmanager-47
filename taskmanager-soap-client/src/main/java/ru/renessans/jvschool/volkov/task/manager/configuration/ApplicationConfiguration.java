package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import ru.renessans.jvschool.volkov.task.manager.api.configuration.IApplicationConfiguration;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.*;

import java.util.concurrent.Executor;

@EnableAsync
@Configuration
@ComponentScan("ru.renessans.jvschool.volkov.task.manager")
public class ApplicationConfiguration implements IApplicationConfiguration {

    @Bean
    @NotNull
    @Override
    public Executor executor() {
        @NotNull final ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(2);
        taskExecutor.setMaxPoolSize(2);
        taskExecutor.setQueueCapacity(500);
        taskExecutor.setThreadNamePrefix("Listener thread-");
        taskExecutor.initialize();
        return taskExecutor;
    }

    @Bean
    @NotNull
    @Override
    public AuthenticationSoapEndpointService authenticationEndpointService() {
        return new AuthenticationSoapEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public ProjectSoapEndpointService projectEndpointService() {
        return new ProjectSoapEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public TaskSoapEndpointService taskEndpointService() {
        return new TaskSoapEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public AuthenticationSoapEndpoint authenticationEndpoint(
            @NotNull final AuthenticationSoapEndpointService authenticationSoapEndpointService
    ) {
        return authenticationSoapEndpointService.getAuthenticationSoapEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public ProjectSoapEndpoint projectEndpoint(
            @NotNull final ProjectSoapEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectSoapEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public TaskSoapEndpoint taskEndpoint(
            @NotNull final TaskSoapEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskSoapEndpointPort();
    }

}