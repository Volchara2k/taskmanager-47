package ru.renessans.jvschool.volkov.task.manager.listener.system;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@Component
@RequiredArgsConstructor
public class ArgumentListListener extends AbstractListener {

    @NotNull
    private static final String CMD_ARGUMENTS = "arguments";

    @NotNull
    private static final String ARG_ARGUMENTS = "-arg";

    @NotNull
    private static final String DESC_ARGUMENTS = "вывод списка поддерживаемых программных аргументов";

    @NotNull
    private static final String NOTIFY_ARGUMENTS = "Список поддерживаемых программных аргументов: \n";

    @NotNull
    private final ICommandService commandService;

    @NotNull
    @Override
    public String command() {
        return CMD_ARGUMENTS;
    }

    @NotNull
    @Override
    public String argument() {
        return ARG_ARGUMENTS;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_ARGUMENTS;
    }

    @Async
    @Override
    @EventListener(condition = "@argumentListListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_ARGUMENTS);
        @Nullable final Collection<String> arguments = this.commandService.getAllArgumentCommands();
        ViewUtil.print(arguments);
    }

}