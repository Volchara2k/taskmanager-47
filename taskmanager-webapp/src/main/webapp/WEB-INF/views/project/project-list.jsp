<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="../../include/_header.jsp" />

<div class="list_header-content">
    <h1>Список проектов:</h1>
</div>

<div class="main-content">
    <table>

        <tr>
            <th width="200" nowrap="nowrap">ID</th>
            <th width="200" nowrap="nowrap">Заголовок</th>
            <th width="100%">Описание</th>
            <th width="150">Статус</th>
            <th width="150">Дата начала</th>
            <th width="150">Дата окончания</th>
            <th width="150" nowrap="nowrap">Действия</th>
        </tr>

        <c:forEach var="project" items="${projects}">
            <tr>
                <td>
                    <c:out value="${project.id}" />
                </td>

                <td>
                    <c:out value="${project.title}" />
                </td>

                <td>
                    <c:out value="${project.description}" />
                </td>

                <td>
                    <c:out value="${project.status.title}" />
                </td>


                <td>
                    <fmt:formatDate value="${project.timeFrame.startDate}" pattern="dd.MM.yyyy" />
                </td>

                <td>
                    <fmt:formatDate value="${project.timeFrame.endDate}" pattern="dd.MM.yyyy" />
                </td>

                <td>
                    <a href="/project/view/${project.id}"><button class="btn btn-outline-primary btn-sm">View</button></a>
                    <a href="/project/edit/${project.id}"><button class="btn btn-outline-primary btn-sm">Edit</button></a>
                    <a href="/project/delete/${project.id}"><button class="btn btn-outline-danger btn-sm">Delete</button></a>
                </td>
            </tr>
        </c:forEach>

    </table>
</div>

<div class="create_link-content">
    <a href="${pageContext.request.contextPath}/project/create/">
        <button class="btn btn-primary btn-sm">Создать</button>
    </a>
</div>

<jsp:include page="../../include/_footer.jsp" />