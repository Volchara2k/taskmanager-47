package ru.renessans.jvschool.volkov.task.manager.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_role")
public final class UserRole extends AbstractModel {

    @NotNull
    @Column(name = "user_role")
    @Enumerated(EnumType.STRING)
    private UserRoleType userRole = UserRoleType.USER;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "mapping_user_id")
    private User user;

    public UserRole(
            @NotNull final User user,
            @NotNull final UserRoleType userRole
    ) {
        this.user = user;
        this.userRole = userRole;
    }

}