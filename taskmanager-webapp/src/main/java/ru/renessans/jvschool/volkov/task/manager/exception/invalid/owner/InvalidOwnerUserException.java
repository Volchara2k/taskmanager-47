package ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidOwnerUserException extends AbstractException {

    @NotNull
    private static final String EMPTY_OBJECT = "Ошибка! Параметр \"объект владельца пользователя\" отсутствует!\n";

    public InvalidOwnerUserException() {
        super(EMPTY_OBJECT);
    }

}