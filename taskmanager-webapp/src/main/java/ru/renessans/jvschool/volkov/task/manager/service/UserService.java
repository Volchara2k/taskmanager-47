package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserRoleService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRoleType;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.*;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.task.manager.model.entity.User;
import ru.renessans.jvschool.volkov.task.manager.model.entity.UserRole;
import ru.renessans.jvschool.volkov.task.manager.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IUserRoleService userRoleService;

    @NotNull
    private final PasswordEncoder passwordEncoder;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final IUserRoleService userRoleService,
            @NotNull final PasswordEncoder passwordEncoder
    ) {
        super(userRepository);
        this.userRepository = userRepository;
        this.userRoleService = userRoleService;
        this.passwordEncoder = passwordEncoder;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        @NotNull final String passwordHash = this.passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash);
        return this.addUser(user, DemoDataConst.USER_ROLE_TYPES);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        @NotNull final String passwordHash = this.passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash, firstName);
        return this.addUser(user, DemoDataConst.USER_ROLE_TYPES);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Set<UserRoleType> userRoleTypes
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        if (Objects.isNull(userRoleTypes)) throw new InvalidUserRoleException();
        @NotNull final String passwordHash = this.passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash);
        return this.addUser(user, userRoleTypes);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final User tempUser,
            @Nullable final Set<UserRoleType> userRoleTypes
    ) {
        if (Objects.isNull(tempUser)) throw new InvalidUserRoleException();
        if (Objects.isNull(userRoleTypes)) throw new InvalidUserRoleException();
        @NotNull final User user = super.save(tempUser);
        @NotNull final Set<UserRole> userRoles = this.userRoleService.addRole(tempUser.getId(), userRoleTypes);
        user.setRoles(userRoles);
        return user;
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public User getUserById(
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        return super.getRecordById(id);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public User getUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        return this.userRepository.getUserByLogin(login);
    }

    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public boolean existsUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        return this.userRepository.existsByLogin(login);
    }

    @Nullable
    @Transactional(readOnly = true)
    @Override
    public Set<UserRole> getUserRoles(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) return null;
        @Nullable final User user = this.getUserById(userId);
        if (Objects.isNull(user)) return null;
        return user.getRoles();
    }

    @NotNull
    @SneakyThrows
    @Override
    public User updateUserPasswordById(
            @Nullable final String id,
            @Nullable final String newPassword
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(newPassword)) throw new InvalidPasswordException();
        @NotNull final String passwordHash = this.passwordEncoder.encode(newPassword);

        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setPasswordHash(passwordHash);

        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User editUserProfileById(
            @Nullable final String id,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setFirstName(firstName);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User editUserProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        if (ValidRuleUtil.isNullOrEmpty(lastName)) throw new InvalidLastNameException();

        @Nullable final User user = this.getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setFirstName(firstName);
        user.setLastName(lastName);

        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User lockUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        @Nullable final User user = this.getUserByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        if (user.getRoles().stream().anyMatch(userRole -> userRole.getUserRole().isAdmin()))
            throw new AccessFailureException();
        user.setLockdown(true);
        return super.save(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User unlockUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        @Nullable final User user = this.getUserByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        if (user.getRoles().stream().anyMatch(userRole -> userRole.getUserRole().isAdmin()))
            throw new AccessFailureException();
        user.setLockdown(false);
        return super.save(user);
    }

    @SneakyThrows
    @Override
    public int deleteUserById(
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        return super.deleteRecordById(id);
    }

    @SneakyThrows
    @Override
    public int deleteUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        return this.userRepository.deleteByLogin(login);
    }

    @PostConstruct
    @NotNull
    @Override
    public Collection<User> initialDemoUsers() {
        @NotNull final Collection<User> nonRewritableResult = new ArrayList<>();

        DemoDataConst.USERS_WITH_UNSECURED_PASSWORD.forEach(user -> {
            @NotNull final String demoLogin = user.getLogin();
            final boolean existsDemoByLogin = this.existsUserByLogin(demoLogin);
            if (!existsDemoByLogin) {
                @NotNull final User initUser = this.initDemoUser(user);
                nonRewritableResult.add(initUser);
            }
        });

        return nonRewritableResult;
    }

    @NotNull
    private User initDemoUser(
            @NotNull final User user
    ) {
        if (DemoDataConst.USER_MANAGER_LOGIN.equals(user.getLogin()))
            return this.addUser(user.getLogin(), user.getPasswordHash(), DemoDataConst.MANAGER_ROLE_TYPES);
        if (DemoDataConst.USER_ADMIN_LOGIN.equals(user.getLogin()))
            return this.addUser(user.getLogin(), user.getPasswordHash(), DemoDataConst.ADMIN_ROLE_TYPES);
        return this.addUser(user.getLogin(), user.getPasswordHash());
    }

}