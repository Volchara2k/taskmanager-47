package ru.renessans.jvschool.volkov.task.manager.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.api.controller.ITaskController;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserProjectService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserTaskService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SecureUserDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserOwnerStatus;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.entity.Task;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public final class TaskController implements ITaskController {

    @NotNull
    private final IUserTaskService userTaskService;

    @NotNull
    private final IUserProjectService userProjectService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @NotNull
    @GetMapping("/tasks")
    @Override
    public ModelAndView index(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-list");
        @NotNull final Collection<Task> tasks = this.userTaskService.getUserOwnerAll(userDTO.getId());
        modelAndView.addObject("tasks", tasks);
        return modelAndView;
    }

    @NotNull
    @GetMapping("/task/create")
    @Override
    public ModelAndView create(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-settable");
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(userDTO.getId());
        modelAndView.addObject("task", taskDTO);
        modelAndView.addObject("statuses", UserOwnerStatus.values());
        @NotNull final Collection<ProjectDTO> projects =
                this.userProjectService.getUserOwnerAll(userDTO.getId())
                        .stream()
                        .map(this.projectAdapterService::toDTO)
                        .collect(Collectors.toList());
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

    @NotNull
    @PostMapping("/task/create")
    @Override
    public ModelAndView create(
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @NotNull final BindingResult result
    ) {
        @Nullable final Task task = this.taskAdapterService.toModel(taskDTO);
        this.userTaskService.addUserOwner(task);
        return new ModelAndView("redirect:/tasks");
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/task/view/{id}")
    @Override
    public ModelAndView view(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Task task = this.userTaskService.getUserOwnerById(userDTO.getId(), id);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-view");
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @Override
    @NotNull
    @GetMapping("/task/delete/{id}")
    public ModelAndView delete(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        this.userTaskService.deleteUserOwnerById(userDTO.getId(), id);
        return new ModelAndView("redirect:/tasks");
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/task/edit/{id}")
    @Override
    public ModelAndView edit(
            @AuthenticationPrincipal @NotNull final SecureUserDTO userDTO,
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Task task = this.userTaskService.getUserOwnerById(userDTO.getId(), id);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        @Nullable final TaskDTO taskDTO = this.taskAdapterService.toDTO(task);
        if (Objects.isNull(taskDTO)) throw new InvalidOwnerUserException();

        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-settable");
        modelAndView.addObject("task", taskDTO);
        modelAndView.addObject("statuses", UserOwnerStatus.values());
        @NotNull final Collection<ProjectDTO> projects =
                this.userProjectService.getUserOwnerAll(userDTO.getId())
                        .stream()
                        .map(this.projectAdapterService::toDTO)
                        .collect(Collectors.toList());
        modelAndView.addObject("projects", projects);

        return modelAndView;
    }

    @NotNull
    @SneakyThrows
    @PostMapping("/task/edit/{id}")
    @Override
    public ModelAndView edit(
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @NotNull final BindingResult result
    ) {
        @Nullable final Task task = this.taskAdapterService.toModel(taskDTO);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        if (ValidRuleUtil.isNullOrEmpty(task.getId())) throw new InvalidIdException();
        this.userTaskService.addUserOwner(task);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task/view/{id}");
        modelAndView.addObject("id", task.getId());
        return modelAndView;
    }

}